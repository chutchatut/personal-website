/** @type {import('tailwindcss').Config} */
export default {
    content: ['./src/**/*.{html,js,svelte,ts}'],
    theme: {
      extend: {}
    },
    plugins: [
      require('tailwindcss-animated')
    ],
    safelist: [
      'bg-[white]',
      'bg-[lightgreen]',
      'bg-[lightgray]',  
      'bg-[lightblue]',
      'bg-[lightred]',
      ...[...Array(20).keys()].map((i)=>`animate-delay-[${i * 150}ms]`)
    ]
  };