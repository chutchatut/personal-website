import MinHeap from "./MinHeap";

export class Border {
  top: boolean;
  bottom: boolean;
  left: boolean;
  right: boolean;

  constructor() {
    this.top = true;
    this.bottom = true;
    this.left = true;
    this.right = true;
  }

  getCSS(): string {
    const borders: string[] = [];
    if (this.top) borders.push("border-t-black");
    if (this.left) borders.push("border-l-black");
    if (this.right) borders.push("border-r-black");
    if (this.bottom) borders.push("border-b-black");
    return borders.join(" ");
  }
}

export const GRID_STATUS = {
  unvisited: "white",
  visited: "lightgray",
  answer: "lightgreen", // is part of the answer
  start: "lightblue",
  end: "lightgreen",
};

export default class {
  size: number;
  arr: string[][];
  walls: Border[][];
  start: number[];
  end: number[];

  constructor(size: number) {
    this.size = size;
    this.arr = [];
    this.walls = [];
    this.start = [0, 0];
    this.end = [0, 0];

    this.reset();
  }

  reset(): void {
    this.start = [Math.floor(Math.random() * 5), Math.floor(Math.random() * 5)];
    this.end = [
      this.size - 1 - Math.floor(Math.random() * 5),
      this.size - 1 - Math.floor(Math.random() * 5),
    ];

    this.arr = this.create2dArray(() => GRID_STATUS.unvisited);
    this.arr[this.start[0]][this.start[1]] = GRID_STATUS.start;
    this.arr[this.end[0]][this.end[1]] = GRID_STATUS.end;

    this.walls = this.create2dArray(() => new Border());
  }

  create2dArray(fill_fn: () => any) {
    return new Array(this.size)
      .fill(0)
      .map(() => new Array(this.size).fill(0).map(fill_fn));
  }

  private inBound(i: number, j: number): boolean {
    return 0 <= i && i < this.size && 0 <= j && j < this.size;
  }

  private shuffle(array: any[]): any[] {
    for (let i = array.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [array[i], array[j]] = [array[j], array[i]];
    }
    return array;
  }

  private recursiveGenerate(
    i: number,
    j: number,
    prev_i: number,
    prev_j: number,
    visited: boolean[][]
  ): void {
    if (!this.inBound(i, j)) return;
    if (visited[i][j]) return;
    visited[i][j] = true;
    if (i != 0 || j != 0) {
      if (i == prev_i) {
        let min_j = Math.min(j, prev_j);
        this.walls[i][min_j].right = false;
        this.walls[i][min_j + 1].left = false;
      } else {
        let min_i = Math.min(i, prev_i);
        this.walls[min_i][j].bottom = false;
        this.walls[min_i + 1][j].top = false;
      }
    }
    let options = [
      [-1, 0],
      [1, 0],
      [0, -1],
      [0, 1],
    ];
    this.shuffle(options).map(([di, dj]) =>
      this.recursiveGenerate(i + di, j + dj, i, j, visited)
    );
  }

  generate(): void {
    // use dfs to recursively generate
    let visited = this.create2dArray(() => false);
    this.recursiveGenerate(0, 0, 0, 0, visited);
  }

  private isUnvisited(i: number, j: number): boolean {
    if (!this.inBound(i, j)) return false;
    return this.arr[i][j] != GRID_STATUS.visited;
  }

  private insertNext(i: number, j: number, cost: number, heap: MinHeap) {
    if (!this.isUnvisited(i, j)) return;
    let [end_x, end_y] = this.end;
    let hue = Math.abs(i - end_x) + Math.abs(j - end_y) + cost;
    heap.add([hue, -cost, i, j]);
  }

  *aStar() {
    let heap = new MinHeap();
    let cost = this.create2dArray(() => null);
    // [hue, distance_to_dest, i, j]

    let [start_x, start_y] = this.start;
    let [end_x, end_y] = this.end;

    heap.add([0, 0, start_x, start_y]);

    // search
    while (true) {
      let [hue, neg_cost, i, j] = heap.peek();
      cost[i][j] = -neg_cost;
      this.arr[i][j] = GRID_STATUS.visited;
      heap.remove();

      // if at the last one, break
      if (i == end_x && j == end_y) break;

      let wall = this.walls[i][j];

      if (!wall.right) {
        this.insertNext(i, j + 1, -neg_cost + 1, heap);
      }
      if (!wall.left) {
        this.insertNext(i, j - 1, -neg_cost + 1, heap);
      }
      if (!wall.bottom) {
        this.insertNext(i + 1, j, -neg_cost + 1, heap);
      }
      if (!wall.top) {
        this.insertNext(i - 1, j, -neg_cost + 1, heap);
      }
      yield;
    }
    // backtrack
    let i = end_x;
    let j = end_y;
    while (i != start_x || j != start_y) {
      this.arr[i][j] = GRID_STATUS.answer;
      let wall = this.walls[i][j];
      if (!wall.right && cost[i][j] - 1 == cost[i][j + 1]) {
        ++j;
      } else if (!wall.left && cost[i][j] - 1 == cost[i][j - 1]) {
        --j;
      } else if (!wall.bottom && cost[i][j] - 1 == cost[i + 1][j]) {
        ++i;
      } else if (!wall.top && cost[i][j] - 1 == cost[i - 1][j]) {
        --i;
      }
      yield;
    }
    this.arr[i][j] = GRID_STATUS.answer;
    yield;
  }
}
