// https://www.geeksforgeeks.org/min-heap-in-javascript/
export default class MinHeap {
  heap: any[];

  constructor() {
    this.heap = [];
  }

  // Helper Methods
  getLeftChildIndex(parentIndex: any) {
    return 2 * parentIndex + 1;
  }
  getRightChildIndex(parentIndex: any) {
    return 2 * parentIndex + 2;
  }
  getParentIndex(childIndex: any) {
    return Math.floor((childIndex - 1) / 2);
  }
  hasLeftChild(index: any) {
    return this.getLeftChildIndex(index) < this.heap.length;
  }
  hasRightChild(index: any) {
    return this.getRightChildIndex(index) < this.heap.length;
  }
  hasParent(index: any) {
    return this.getParentIndex(index) >= 0;
  }
  leftChild(index: any) {
    return this.heap[this.getLeftChildIndex(index)];
  }
  rightChild(index: any) {
    return this.heap[this.getRightChildIndex(index)];
  }
  parent(index: any) {
    return this.heap[this.getParentIndex(index)];
  }

  // Functions to create Min Heap

  swap(indexOne: any, indexTwo: any) {
    const temp = this.heap[indexOne];
    this.heap[indexOne] = this.heap[indexTwo];
    this.heap[indexTwo] = temp;
  }

  peek() {
    if (this.heap.length === 0) {
      return null;
    }
    return this.heap[0];
  }

  // Removing an element will remove the
  // top element with highest priority then
  // heapifyDown will be called
  remove() {
    if (this.heap.length === 0) {
      return null;
    }
    const item = this.heap[0];
    this.heap[0] = this.heap[this.heap.length - 1];
    this.heap.pop();
    this.heapifyDown();
    return item;
  }

  add(item: any) {
    this.heap.push(item);
    this.heapifyUp();
  }

  heapifyUp() {
    let index = this.heap.length - 1;
    while (this.hasParent(index) && this.parent(index) > this.heap[index]) {
      this.swap(this.getParentIndex(index), index);
      index = this.getParentIndex(index);
    }
  }

  heapifyDown() {
    let index = 0;
    while (this.hasLeftChild(index)) {
      let smallerChildIndex = this.getLeftChildIndex(index);
      if (
        this.hasRightChild(index) &&
        this.rightChild(index) < this.leftChild(index)
      ) {
        smallerChildIndex = this.getRightChildIndex(index);
      }
      if (this.heap[index] < this.heap[smallerChildIndex]) {
        break;
      } else {
        this.swap(index, smallerChildIndex);
      }
      index = smallerChildIndex;
    }
  }

  printHeap() {
    var heap = ` ${this.heap[0]} `;
    for (var i = 1; i < this.heap.length; i++) {
      heap += ` ${this.heap[i]} `;
    }
  }
}
